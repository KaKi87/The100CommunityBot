# The 100 Community Bot

The must-have bot for any show-based Discord server
(especially [The 100 Community's Discord server](https://discord.gg/658TgTV)).

*Disclaimer : some of this bot's features are specific to The 100,
especially RP. These parts will not be documented on this README,
but may be in the wiki.*

## Getting started

### Prerequisites
- NodeJS
- Yarn

### Installing
```
yarn install
yarn start
```

### Setting up (config file)

Copy [config.example.json](config.example.json) as `config.json`.

#### Name & copyright
You're free to give any name you want to your bot instance :
the full `name` will be used in the help embed,
`shortName` will be used as bot's username (or `name` if not set).

A copyright is shown at end of every embed,
from the [package.json](package.json)'s author field :
please fork and contribute to the project if you want to change it,
and in that case, change the `config.year` (fork year) if needed.

#### Guild
1 bot instance = 1 `guild` (fill field with IDs).

#### Channels
- `general` : for welcome and goodbye messages (both optional).
- `show_news` : TBD
- `release` : for PreDB release notifications (optional)
- `logs` : **required** for logs

#### Roles
- `admin` : **required** for sensitive commands.
- `mod` : TBD
- `mute` : if you have one

#### Seasons self-roles

If you (want to) have per-season channels and roles,
the bot will automatically create them, and assign by command.
Set `categories.seasons` as the category ID containing the seasons channels.

**Channel & role naming format : `season-<n>`**

#### TVMaze (required)
Look for your show on [TVMaze](https://www.tvmaze.com/) and get its
ID from the URL : tvmaze.com/shows/**id**.
Put that number in the `show.tvMazeID` field.

**⚠ Required for the show features**
(otherwise you don't need that bot xD)

#### AllDebrid
If you want to provide premium host download links using
[AllDebrid](https://alldebrid.com/), fill `id` and `pass` in `show.alldebrid`.

Then, insert your links in `data/downloads.json` using the following syntax :
```json
{
	"season": {
		"episode": {
			"release_name": [
				"link"
			]
		}
	}
}
```
You can set multiple episodes (the user will have the choice)
and multiple links, preferably from multiple hosters (they will be tested
requentially until the link generation work)

⚠ AllDebrid blocks API requests from dedicated servers.

#### PreDB
If you want to be notified when a new release of your show
is available on warez websites, PreDB is the solution.
Just enter the beginning of the release name containing the series' name
(for example : `The.100`) in the `show.releaseName` field.

#### Welcome & goodbye messages
Type your automated messages in the `messages.welcome` and
`messages.goodbye` fields, with `%m` as the new/old member mention.

**⚠ The _general_ channel is required for that feature to work.**

#### Topics-based self-roles

If you (want to) topic-based channels, add them as an object into
the `topics` array with the following properties :
- `category` : the channels category
- `name` : the topic name
- `max` : *(optional)* maximum roles per user for this category
- `joinCommand` : command to get a role (without the prefix)
- `leaveCommand` : command to remove a role (without the prefix)
- `joinMessage` : *(optional)* announcement when user join
- `leaveMessage` : *(optional)* announcement when user leaves
- `sort` : *(optional)* stats sorting mode
	- `position` : *(default)* channel position in Discord's list
	- `name` : channel name
	- `members` : members count

#### Misc

##### Embed color

You can set a fixed color for the bot's embed in `embedColor`, or specify
`null` for random.

##### Reveal spoilers

Set either `true` or `false` wether you want or not enable the spoilers
revealer feature. The only purpose is frustrate people, which makes them
loose the interest in abusing of it :p

##### Owner

Optional because relative to advanced developer commands.

## Built With
Node modules :
- [console-stamp](https://github.com/starak/node-console-stamp) - Timestamped logging
- [cron](https://github.com/kelektiv/node-cron) - Task scheduler
- [Discord.JS](https://expressjs.com/) - Discord bot API
- [requestretry](https://github.com/FGRibreau/node-request-retry) - HTTP requests with auto-retry (based on [request](https://github.com/request/request))
- [table](https://www.npmjs.com/package/table) - ASCII table output
- [TVMaze](https://github.com/jonhester/tvmaze) - TVMaze API

## Bot commands

Index for global features only.

### TVMaze
- `episode <SXXEXX>` : get episode info
- `last` : get last episode aired
- `next` : get next episode to be aired
- `subscribe <D-days> <hour> [00/15/30/45]` : subscribe to episode
release notifications

### PreDB
- `episode <SXXEXX> releases` : get episode releases

### Seasons
- `seasons` : show seasons listing
- `season <season>` : set season role (grant access to channels)

### Self-roles

- `self` : roles list
- `stats <self>` : show stats for category

	\+ user-defined join & leave commands
	
**⚠ Roles name must be identical to channels name** (but, case insensitive).

### Eval

Only work in DM by `owner`.

Syntax :

	
	eval
	```js
	message.channel.send('Hello, world !');
	```

Don't forget the prefix before `eval`.

## Authors
**KaKi87 (Tiana Lemesle)** - *Initial work*

## License
This project is licensed under the GNU GPLv2 License
- see the [LICENSE](LICENSE) file for details