require('console-stamp')(console, {
	pattern: 'dd/mm/yy HH:MM:ss.l',
	colors: {
		stamp: 'yellow',
		label: 'green'
	}
});

const config = require('./config.json'),
	_package = require('./package.json');

const Discord = require('discord.js'),
	    table = require('table').table;

const Client = new Discord.Client();

const   utils = require('./includes/utils.js'),
        embed = require('./includes/embed.js'),
	  Episode = require('./includes/Episode.js'),
	     show = require('./includes/show.js'),
	    predb = require('./includes/predb.js'),
subscriptions = require('./includes/subscriptions.js'),
	   Select = require('./includes/Select.js'),
	reactions = require('./includes/reactions.js');

let Guild = null,
	seasonsRoles = [];

Client.on('ready', () => {
	console.log('Ready');
	Client.user.setUsername(config.shortName || config.name).catch(err => console.error(err));
	Client.user.setPresence({ game: { name: `${config.prefix}help` }, status: 'online' }).catch(err => console.error(err));
	Guild = Client.guilds.get(config.guild);
	Client.guilds.forEach(guild => {
		if(guild.id !== Guild.id)
			guild.leave().then(() => console.log(`[!] Leaved unauthorized guild ${guild.name} (${guild.id})`));
	});
	if(!Guild){
		console.error('Guild not joined or invalid ID');
		return shutdown();
	}
	subscriptions.init(Client);
	{
		let checkPreDB = () => {
			console.log('Checking PreDB');
			predb.checkNew(res => {
				console.log('PreDB checked');
				let next = () => setTimeout(() => checkPreDB(), 900000); // 15 mins
				if(!res) return next();
				Guild.channels.get(config.channels.release)
					.send(`New release${res.length > 1 ? 's' : ''} available : ${utils.codeBlock(res.join('\n'))}`)
					.then(() => next());
			});
		};
		checkPreDB();
	}
	if(config.categories.seasons && !seasonsRoles.length){
		show.getAllSeasonsNumbers(res => {
			(async() => {
				for(let i = res.length - 1; i >= 0; i--){
					let name = `season-${res[i]}`,
						role = Guild.roles.find(role => name === role.name);
					if(!role){
						await Guild.createRole({ name }, 'Season channel').then(newRole => {
							role = newRole;
							console.log(`Created role "${role.name}"`);
						});
					}
					seasonsRoles.push(role);
				}
				for(let i = res.length - 1; i >= 0; i--){
					let name = `season-${res[res.length - i - 1]}`,
						channel = Guild.channels.find(channel => name === channel.name && channel.parentID === config.categories.seasons);
					if(!channel){
						await Guild.createChannel(name, 'text').then(newChannel => {
							console.log(`Created channel #${newChannel.name}`);
							channel = newChannel;
						});
					}
					let permissionsOverwrites = [{ id: Guild.id, denied: ['VIEW_CHANNEL'] }];
					if(config.roles.mute){
						permissionsOverwrites.push({ id: config.roles.mute, denied: ['SEND_MESSAGES', 'ADD_REACTIONS', 'SPEAK'] });
					}
					channel.replacePermissionOverwrites({
						overwrites: permissionsOverwrites
							.concat(seasonsRoles.slice(0, i+1).map(role => ({ id: role.id, allowed: ['VIEW_CHANNEL']})))
					}).then(() => {
						channel.setParent(config.categories.seasons).then(() => {
							console.log(`Permissions reset for #${channel.name}`);
						});
					});
				}
			})();
		});
	}
});

if(config.channels.general){
	if(config.messages.welcome){
		Client.on('guildMemberAdd', member => {
			if(member.user.bot) return console.log(`Bot ${member.user.username} invited`);
			Guild.channels.get(config.channels.general).send(config.messages.welcome.replace('%m', utils.mention(member.id)));
		});
	}
	if(config.messages.goodbye){
		Client.on('guildMemberRemove', member => {
			if(member.user.bot) return;
			let goodbyeMessage = config.messages.goodbye.replace('%m', utils.mention(member.user, true));
			Guild.channels.get(config.channels.general).send(goodbyeMessage);
			member.send(goodbyeMessage)
				.catch(err => {
					if(err.code !== 50007) // Cannot send DM if no common server
						console.error(err);
				});
		});
	}
}

let revealSpoiler = message => {
	if(!config.revealSpoilers || message.content.startsWith(config.prefix)) return;
	if(new RegExp(/.*\|\|.*\|\|.*/).test(message.content)){
		message.channel.send(`${utils.mention(message.author.id)} said : ${utils.code(message.content.replace(/\|\|/g, ''))}`);
		return true;
	}
	let attachments = message.attachments.array();
	if(attachments[0] && attachments[0].filename.startsWith('SPOILER_')){
		message.channel.send(`${utils.mention(message.author.id)} uploaded : ${attachments[0].url}`);
		return true;
	}
};

Client.on('message', message => {
	// if(message.author.id === Client.user.id) return;
	/*
	Anti-Spam : fix high delay
	message.channel.fetchMessages({ limit: 2 }).then(messages => {
		if(messages.map(msg => msg.author.id).every((v, i, a) => v === a[0]))
			message.delete();
	});
	*/
	if(revealSpoiler(message)) return;
	if(!message.content.startsWith(config.prefix)) return;
	const badCommand = () => message.channel.send(`:x: Bad command. Type ${utils.code(`${config.prefix}help`)}.`);
	const msg = message.content.slice(config.prefix.length).toLowerCase();
	const cmd = msg.split(/\s/)[0];
	const args = msg.split(/\s/).slice(1);
	const args_string = args.join(' ');
	const args_assoc = {};
	args.forEach(param => {
		let kv = new RegExp(/^([a-z]+)=(.+)$/i).exec(param);
		if(kv) args_assoc[kv[1]] = kv[2];
	});
	const isSet = param => args.indexOf(param) !== -1 || args_assoc[param] !== undefined;
	console.log(`CMD ► ${message.author.username}#${message.author.discriminator} "${cmd}" ${JSON.stringify(args)}`);
	if(config.topics.map(t => t['name']).concat(config.topics.map(t => t['joinCommand'])).concat(config.topics.map(t => t['leaveCommand'])).indexOf(cmd) !== -1){
		config.topics.forEach(t => {
			if([ t['name'], t['joinCommand'], t['leaveCommand'] ].indexOf(cmd) === -1) return;
			let channels = Guild.channels.filter(channel => channel.parentID === t['category']);
			let roles = [];
			channels.forEach(channel => roles.push(Guild.roles.find(c => c.name.toLowerCase() === channel.name)));
			let userTopicRoles = utils.intersect(message.member.roles.map(r => r.id), roles.map(r => r.id))
				.map(r => Guild.roles.find(_r => r === _r.id));
			if(cmd === t['name']){
				message.channel.send(embed(Client, {
					title: utils.capitalize(t['name']),
					description: `${utils.code(`${config.prefix}${t['joinCommand']}`)} | ${utils.code(`${config.prefix}${t['leaveCommand']}`)}`,
					fields: [
						{
							name: 'Roles list',
							value: `- ${channels.map(c => c.name).map(r => utils.capitalize(r)).join('\n- ')}`
						},
						{
							name: 'Roles limit',
							value: t['max'] > 0 ? `You can only have **${t['max']}** of these roles at a time.` : `There is no limit on this category.`
						}
					]
				}));
			}
			else {
				if(!args[0])
					return message.channel.send(':x: You must provide a role.');
				let targetRole = Guild.roles.find(r => r.name.toLowerCase() === args[0]),
					targetChannel = Guild.channels.find(c => c.name === args[0]);
				if(!targetRole)
					return message.channel.send(':x: There is no such role.');
				if(cmd === t['joinCommand']){
					if(userTopicRoles.map(r => r.name.toLowerCase()).indexOf(args[0]) !== -1){
						return message.channel.send(':x: You already have that role.');
					}
					if(t['max'] > 0 && userTopicRoles.length >= t['max']){
						return message.channel.send(`You already have the following role(s) :\n- ${userTopicRoles.map(r => r.name).join('\n- ')}\nYou must ${utils.code(`${config.prefix}${t['leaveCommand']}`)} them first.`);
					}
					message.member.addRole(targetRole)
						.then(() => {
							message.channel.send(`:white_check_mark: ${targetRole.name} successfully added.`);
							let joinMessage = `Welcome ${utils.mention(message.author.id)}`;
							if(t['joinMessage'])
								joinMessage = t['joinMessage'].replace('%m', utils.mention(message.author.id));
							targetChannel.send(joinMessage);
						})
						.catch(err => {
							console.error(err);
							message.channel.send(':x: Error.');
						});
				}
				else if(cmd === t['leaveCommand']){
					if(userTopicRoles.map(r => r.name.toLowerCase()).indexOf(args[0]) === -1)
						return message.channel.send(`:x: You don't have that role.`);
					message.member.removeRole(targetRole)
						.then(() => {
							message.channel.send(`:white_check_mark: ${targetRole.name} successfully removed.`);
							let leaveMessage = `Good bye ${utils.mention(message.author.id)}`;
							if(t['leaveMessage'])
								leaveMessage = t['leaveMessage'].replace('%m', utils.mention(message.author.id));
							targetChannel.send(leaveMessage);
						})
						.catch(err => {
							console.error(err);
							message.channel.send(':x: Error.');
						});
				}
			}
		});
		return;
	}
	switch(cmd){
		case 'episode':
			{
				let regex = /^s([0-9]+)e([0-9]+)$/;
				if(!args[0] || !new RegExp(regex).test(args[0]))
					return badCommand();
				let match = args[0].match(regex).map(m => parseInt(m));
				switch(args[1]){
					case 'releases':
						let response = '';
						args[0] = args[0].toUpperCase();
						predb.search(`${config.show.releaseName}.${args[0]}`, res => {
							if(!res)
								response = `:negative_squared_cross_mark: No release available for ${args[0]}`;
							else {
								response = `:notepad_spiral: Available releases for ${args[0]} : ${utils.codeBlock(res.map(r => r['name']).join('\n'))}`;
							}
							message.channel.send(response);
						});
						break;
					case 'info':
					case undefined:
						let ep = new Episode(match[1], match[2], Client, message.author.id);
						ep.getData(() => {
							ep.generateEmbed();
							ep.postEmbed(Client, message.channel);
						});
						break;
				}
			}
			break;
		case 'last':
			{
				let ep = new Episode(null, null, Client, message.author.id);
				show.getLastEpisode(episode => {
					ep.updateData(episode);
					ep.generateEmbed();
					ep.postEmbed(Client, message.channel);
				});
			}
			break;
		case 'next':
			{
				let ep = new Episode(null, null, Client, message.author.id);
				show.getFutureEpisode(episode => {
					ep.updateData(episode);
					ep.generateEmbed();
					ep.postEmbed(Client, message.channel);
				});
			}
			break;
		case 'countdown':
			{
				const secToDHHMMSS = seconds => `${parseInt(seconds / 86400)}d ${new Date(seconds * 1000).toISOString().substr(11, 8)}`;
				show.getFutureEpisode(episode => {
					message.channel.send(`:stopwatch: Countdown to next episode : ${!episode['airstamp'] ? 'unknown' : secToDHHMMSS((new Date(episode['airstamp']).getTime() - Date.now()) / 1000)}`)
				});
			}
			break;
		case 'trigedasleng':
			message.channel.send(embed(Client, {
				title: 'Trigedasleng info',
				fields: [
					{
						name: 'Language reference',
						value: '[trigedasleng.net](https://trigedasleng.net/) ~~[trigedasleng.info](http://trigedasleng.info/)~~'
					},
					{
						name: 'Translator',
						value: '[lingojam.com](https://lingojam.com/TrigedaslengTranslator)'
					}
				]
			}));
			break;
		case 'subscriptions':
			{
				let s = subscriptions.get(message.author.id);
				if(!s) return message.channel.send(`You don't have subscribed any reminder.`);
				let fields = [];
				for(const [d, t] of Object.entries(s)){
					fields.push({ name: `D-${d}`, value: `• ${t.join('\n• ')}` });
				}
				fields.push({ name: '** **', value: '24h, UTC' });
				message.channel.send(embed(Client, {
					title: 'Your subscriptions', fields
				}));
			}
			break;
		case 'subscribe':
			if(
				(!args[0] || isNaN(parseInt(args[0])))
				||
				(!args[1] || isNaN(parseInt(args[1])))
				||
				(args[2] && ['00', '15', '30', '45'].indexOf(args[2]) === -1)
			)
			{
				return badCommand();
			}
			if(subscriptions.subscribe(message.author.id, args[0], `${utils.twoDigits(parseInt(args[1]))}:${args[2] || '00'}`)){
				message.channel.send('Subscription confirmed.');
			}
			else {
				message.channel.send('You already have subscribed for this date and time period.');
			}
			break;
		case 'unsubscribe':
			if(
				(!args[0] || isNaN(parseInt(args[0])))
				||
				(!args[1] || isNaN(parseInt(args[1])))
				||
				(args[2] && ['00', '15', '30', '45'].indexOf(args[2]) === -1)
			)
			{
				return badCommand();
			}
			let response = '';
			switch(subscriptions.unsubscribe(message.author.id, parseInt(args[0]), `${utils.twoDigits(parseInt(args[1]))}:${args[2] || '00'}`)){
				case 0:
					response = 'Successfully unsubscribed.';
					break;
				case 1:
					response = `You don't have subscribed for this time period at this date.`;
					break;
				case 2:
					response = `You don't have any subscription for this date period.`;
					break;
				case 3:
					response = `You don't have any subscription yet.`;
					break;
			}
			message.channel.send(response);
			break;
		case 'seasons':
			let user = message.member.id;
			let seasons = seasonsRoles.map(r => r.name.slice(7));
			let output = `- ${seasons.join('\n- ')}`;
			if(message.author.id === Client.user.id && args[0]){
				user = args[0];
				output = `${seasonsRoles.map((r, i) => `${utils.code(`${i+1}`)} ► ${r.name}`).join('\n')}`;
			}
			if(message.author.id === Client.user.id){
				new Select(Client, message.channel, args[0],
					seasons, null,
					() => message.channel.send('Season select canceled'),
					selected => message.channel.send(`${config.prefix}season ${selected} ${args[0]}`));
			}
			else {
				message.channel.send(embed(Client, {
					title: '',
					fields: [
						{
							name: 'Seasons roles',
							value: output
						}
					]
				}));
			}
			break;
		case 'season':
			{
				if(!args[0]) return badCommand();
				let user = message.member;
				let auto = false;
				let role = Guild.roles.find(role => role.name === `season-${args[0]}`);
				if(message.author.id === Client.user.id && args[1]){
					user = Guild.members.get(args[1]);
					auto = true;
				}
				user.removeRoles(seasonsRoles.map(r => r.id)).then(() => {
					if(args[0] === 'none')
						return message.channel.send(':white_check_mark: All seasons removed.');
					if(!role) return message.channel.send(`:x: This season doesn't exist.`);
					user.addRole(role.id).then(() => {
						message.channel.send(':white_check_mark: Season role assigned successfully.');
					});
				});
			}
			break;
		case 'self':
			message.channel.send(`**Self roles list**\n\n- ${utils.code(`${config.prefix}${config.topics.map(t => t['name']).join(`${utils.code(`\n- `)}${config.prefix}`)}`)}`);
			break;
		case 'stats':
			if(!args[0]) return badCommand();
			let topic = config.topics.find(topic => topic.name === args[0]);
			let category = null;
			if(args[0] === 'seasons')
				category = Guild.channels.find(channel => channel.id === config.categories.seasons);
			else
				category = Guild.channels.find(channel => channel.id === topic['category']);
			if(!category)
				return message.channel.send(':x: There is no such category.');
			let channels = Guild.channels.filter(channel => channel.parentID === category.id);
			let roles = [];
			let users = new Set();
			Guild.fetchMembers().then(() => {
				channels.forEach(c => {
					let role = Guild.roles.find(r => r.name.toLowerCase() === c.name);
					roles.push(role);
					users = new Set([...users, ...role.members.filter(member => !member.bot).map(u => u.id)]);
				});
				for(let i = 0; i < roles.length; i++){
					roles[i].members = roles[i].memberCount = roles[i].members.filter(member => !member.bot).array().length
				}
				if(topic){
					switch(topic['sort']){
						case 'name':
							roles = roles.sort((a, b) => a.name < b.name ? -1 : 1);
							break;
						case 'members':
							roles = roles.sort((a, b) => b.memberCount - a.memberCount);
							break;
						case 'position':
						default:
							roles = roles.sort((a, b) => a.position - b.position);
							break;
					}
				}
				else
					roles = roles.sort((a, b) => a.position - b.position);
				message.channel.send(`Stats for **${args[0]}** :\n${utils.codeBlock(table([ ['ROLE', 'MEMBER COUNT'] ]
					.concat(roles.map(role => [`${role.name}`, `${role.memberCount}`]))
					.concat([ [ 'TOTAL', `${users.size} (${(users.size / Guild.memberCount * 100).toFixed(2)}%)` ] ])))}`);
			});
			break;
		case 'help':
			{
				message.channel.send(embed(Client, {
					title: `General commands\n** **`,
					description: `${utils.code(`${config.prefix}cmd <required_param> [optional] [DEFAULT_CHOICE/other_choice]`)}\n**Shortcut (fast commands) ** : ${utils.code(config.prefix)}`,
					fields: [
						{
							name: 'Get episode',
							value: utils.code(`${config.prefix}episode <SXXEXX> [INFO/releases]`),
							inline: true
						},
						{
							name: 'Get last episode aired',
							value: utils.code(`${config.prefix}last`),
							inline: true
						},
						'margin',
						{
							name: 'Get next episode to be aired',
							value: utils.code(`${config.prefix}next`),
							inline: true
						},
						{
							name: 'Countdown to next episode',
							value: utils.code(`${config.prefix}countdown`),
							inline: true
						},
						'margin',
						{
							name: 'Seasons list',
							value: utils.code(`${config.prefix}seasons`),
							inline: true,
							condition: () => seasonsRoles.length > 0
						},
						{
							name: 'Join season channel',
							value: utils.code(`${config.prefix}season [<season>|none]`),
							inline: true,
							condition: () => seasonsRoles.length > 0
						},
						'margin',
						{
							name: 'Trigedasleng info',
							value: utils.code(`${config.prefix}trigedasleng`),
							inline: true,
							condition: () => Guild.id === '242022034257870849'
						},
						{
							name: 'Self-roles list',
							value: utils.code(`${config.prefix}self`),
							inline: true,
							condition: () => config.topics.length > 0
						},
						{
							name: 'Self-roles stats',
							value: utils.code(`${config.prefix}stats <self-topic|seasons>`),
							inline: true,
							condition: () => config.topics.length > 0
						},
						'margin',
						{
							name: '*(un)*Subscribe reminders',
							value: `${utils.code(`${config.prefix}(un)subscribe <D-days> <hour> [00/15/30/45]`)} **24h, UTC**
									Timezone conversion : <https://www.worldtimebuddy.com/>`
						},
						'margin',
						{
							name: 'Shutdown **(admin)**',
							value: utils.code(`${config.prefix}shutdown`),
							condition: () => message.member.roles.has(config.roles.admin)
						},
						'margin',
						{
							name: '** **',
							value: `[GitHub](${_package.repository.url})`
						}
					]
				}));
			}
			break;
		case 'shutdown':
			if(message.member.roles.has(config.roles.admin)){
				message.channel.send('Shutting down');
				shutdown();
			}
			else {
				Guild.channels.get(config.channels.logs).send(`Unallowed shutdown attempt from ${utils.mention(message.author.id)}`);
			}
			break;
		case 'eval':
			if(message.author.id === config.owner && message.channel.type === 'dm'){
				let cmd = new RegExp("^" + config.prefix.replace(/(.)/g, '\\$1') + "eval\\n```js\\n(.+)\\n```$", "s").exec(message.content)[1];
				try {
					message.channel.send(utils.codeBlock(eval(cmd)));
				} catch(e) {
					message.channel.send(utils.codeBlock(e));
				}
			}
			break;
		default:
			if(cmd) return message.channel.send(`Command not found. Type ${utils.code(`${config.prefix}help`)}.`);
			message.channel.send(embed(Client, {
				title: 'Fast commands\n** **',
				description: 'React to execute command',
				fields: [
					{
						name: '** **',
						value: `
							:information_source: Full help
							
							:track_previous: Get last episode aired
							
							:track_next: Get next episode to be aired
							
							:hourglass: Countdown to next episode
							
							:speech_balloon: Join season channel
							
							:globe_with_meridians: Trigedasleng info
							
							:clipboard: Self-roles list
						`
					},
					'margin'
				]
			})).then(msg => {
				reactions.add(msg, ['ℹ', '⏮', '⏭', '⌛', '💬', '🌐', '📋'], () => {
					let emojis = ['ℹ', '⏮', '⏭', '⌛', '🌐', '📋'];
					let commands = ['help', 'last', 'next', 'countdown', 'trigedasleng', 'self'];
					let h = {};
					emojis.forEach((emoji, index) => {
						h[emoji] = {
							action: () => {
								message.channel.send(`${config.prefix}${commands[index]}`)
									.then(msg2 => msg.delete().then(() => msg2.delete()))
							},
							user: message.author.id
						};
					});
					h['💬'] = {
						action: () => {
							message.channel.send(`${config.prefix}seasons ${message.author.id}`)
								.then(msg2 => msg.delete().then(() => msg2.delete()))
						},
						user: message.author.id
					};
					reactions.on(msg.id, h);
				});
			});
			break;
	}
});

Client.on('messageUpdate', (oldMessage, newMessage) => {
	revealSpoiler(newMessage);
});

Client.on('messageReactionAdd', (reaction, user) => {
	if(user.bot) return;
	reactions.handle(reaction.message.id, reaction.emoji.toString(), user.id, () => {
		reaction.remove(user);
	});
});

Client.login(config.token)
	.then(() => {
		console.log('Logged in');
	})
	.catch(err => {
		console.error(err);
		process.exit(0);
	});

Client.on('guildCreate', guild => {
	guild.leave().then(() => console.log(`[!] Leaved unauthorized guild ${guild.name} (${guild.id})`));
});

Client.on('error', err => console.error(err));

process.setMaxListeners(0);

process.on('uncaughtException', err => console.error(err.stack));

process.on('unhandledRejection', err => console.error(`Uncaught Promise Rejection: \n${err.stack}`));

let shutdown = () => {
	console.log('Disconnecting');
	Client.destroy()
		.then(() => {
			console.log('Disconnected');
			process.exit(0);
		})
		.catch(err => {
			console.error(err);
			process.exit(1);
		})
};

process.on('SIGINT', () => shutdown());
