const reactions = require('./reactions.js');

const empty = '** **';
const n = ["\u0030\u20E3","\u0031\u20E3","\u0032\u20E3","\u0033\u20E3","\u0034\u20E3","\u0035\u20E3", "\u0036\u20E3","\u0037\u20E3","\u0038\u20E3","\u0039\u20E3"];

module.exports = class Numpad {
	constructor(Client, channel, commandUser, hiddenInput = false, maxLength = Infinity, onSubmit, onCancel, mode){
		this.channel = channel;
		this.commandUser = commandUser;
		this.hiddenInput = hiddenInput;
		this.maxLength = maxLength;
		this.submit = onSubmit;
		this.cancel = onCancel;
		this.mode = mode;
		this.input = '';
		this.rows = [];
		this.createInput();
	}

	createInput(){
		this.channel.send('Wait').then(row0 => {
			this.rows[0] = row0;
			this.channel.send(empty).then(row1 => {
				this.rows[1] = row1;
				this.channel.send(empty).then(row2 => {
					this.rows[2] = row2;
					this.channel.send(empty).then(row3 => {
						this.rows[3] = row3;
						this.channel.send(empty).then(row4 => {
							this.rows[4] = row4;
							let n13 = [ n[1], n[2], n[3] ];
							let n46 = [ n[4], n[5], n[6] ];
							let n79 = [ n[7], n[8], n[9] ];
							let x0v = [ '❌', n[0], '✅' ];
							if(this.mode === 'calc'){
								n13.push('➗');
								n46.push('✖');
								n79.push('➖');
								x0v.push('➕');
							}
							reactions.add(this.rows[1], n13, () => {
								reactions.add(this.rows[2], n46, () => {
									reactions.add(this.rows[3], n79, () => {
										reactions.add(this.rows[4], x0v, () => {
											// ------------------
											let r1 = {};
											r1[n[1]] = { action: cb => this.typeInput('1', cb), remove: true, user: this.commandUser, cb: true };
											r1[n[2]] = { action: cb => this.typeInput('2', cb), remove: true, user: this.commandUser, cb: true };
											r1[n[3]] = { action: cb => this.typeInput('3', cb), remove: true, user: this.commandUser, cb: true };
											r1['➗'] = { action: cb => this.typeInput('/', cb), remove: true, user: this.commandUser, cb: true };
											reactions.on(this.rows[1].id, r1);
											let r2 = {};
											r2[n[4]] = { action: cb => this.typeInput('4', cb), remove: true, user: this.commandUser, cb: true };
											r2[n[5]] = { action: cb => this.typeInput('5', cb), remove: true, user: this.commandUser, cb: true };
											r2[n[6]] = { action: cb => this.typeInput('6', cb), remove: true, user: this.commandUser, cb: true };
											r2['✖'] = { action: cb => this.typeInput('*', cb), remove: true, user: this.commandUser, cb: true };
											reactions.on(this.rows[2].id, r2);
											let r3 = {};
											r3[n[7]] = { action: cb => this.typeInput('7', cb), remove: true, user: this.commandUser, cb: true };
											r3[n[8]] = { action: cb => this.typeInput('8', cb), remove: true, user: this.commandUser, cb: true };
											r3[n[9]] = { action: cb => this.typeInput('9', cb), remove: true, user: this.commandUser, cb: true };
											r3['➖'] = { action: cb => this.typeInput('-', cb), remove: true, user: this.commandUser, cb: true };
											reactions.on(this.rows[3].id, r3);
											let r4 = {
												'❌': {
													action: cb => {
														if(this.input.length > 0)
															this.emptyInput(cb);
														else
															this.cancelInput();
													},
													remove: true,
													user: this.commandUser,
													cb: true
												},
												'✅': { action: () => this.submitInput(), remove: true, user: this.commandUser },
												'➕': { action: cb => this.typeInput('+', cb), remove: true, user: this.commandUser, cb: true }
											};
											r4[n[0]] = { action: cb => this.typeInput('0', cb), remove: true, user: this.commandUser, cb: true };
											reactions.on(this.rows[4].id, r4);
											this.rows[0].edit(`Input : \`> ${this.maxLength !== Infinity ? `[${' '.repeat(this.maxLength)}]` : '_'}\``)
												.catch(err => console.error(err));
											// ------------------
										});
									});
								});
							});
						});
					});
				});
			});
		});
	};

	updateInput(cb){
		this.input = this.input.substr(0, this.maxLength);
		this.rows[0].edit(`Input : \`> ${this.maxLength !== Infinity ? '[' : ''}${this.hiddenInput ? this.input.replace(/./g, '•') : this.input}${this.maxLength !== Infinity ? `${' '.repeat(this.maxLength - this.input.length)}]` : '_'}\``)
			.then(() => { if(cb) cb(); })
			.catch(err => console.error(err));
	};

	typeInput(c, cb){
		this.input += c;
		this.updateInput(cb);
	}

	emptyInput(c, cb){
		this.input = '';
		this.updateInput(cb);
	}

	deleteInput(callback){
		this.rows[0].delete().then(() => {
			this.rows[1].delete().then(() => {
				this.rows[2].delete().then(() => {
					this.rows[3].delete().then(() => {
						this.rows[4].delete().then(() => {
							if(callback)
								callback();
						});
					});
				});
			});
		});
	}

	cancelInput(){
		this.deleteInput(() => {
			this.cancel();
		});
	}

	submitInput(){
		if(this.input.length === 0) return;
		this.deleteInput(() => {
			this.submit(this.input);
		});
	};
};