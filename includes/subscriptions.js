const fs = require('fs'),
	CronJob = require('cron').CronJob;

const utils = require('./utils.js'),
	   show = require('./show.js'),
	 config = require('../config.json'),
	Episode = require('./Episode.js');

const dataPath = `./data/subscriptions.json`;

let getSubscriptions = () => JSON.parse(fs.readFileSync(dataPath, 'utf-8') || {});
let setSubscriptions = data => {
	fs.writeFileSync(dataPath, JSON.stringify(data, null, '\t'));
	console.log('Subscriptions saved');
};


module.exports = {
	init: Client => {
		let Guild = Client.guilds.get(config.guild);
		new CronJob('0,15,30,45 * * * *', () => {
			let d = new Date(),
				h = utils.twoDigits(d.getUTCHours()),
				m = utils.twoDigits(d.getUTCMinutes());
			show.getFutureEpisode(episode => {
				let ep = new Episode(null, null, Client, null);
				ep.updateData(episode);
				ep.generateEmbed();
				if(!episode['airstamp']) return;
				let daysLeft = parseInt((new Date(episode['airstamp']).getTime() - Date.now()) / 1000 / 86400).toString();
				for(const [user, dates] of Object.entries(getSubscriptions())){
					if(dates[daysLeft] && dates[daysLeft].find(t => t === `${h}:${m}`) && Client){
						Guild.fetchMember(user).then(user => user.send(config.messages.reminder, ep.embed));
					}
				}
			});
		}, null, true);
	},
	subscribe: (user, d, t) => {
		let subscriptions = getSubscriptions();
		if(!subscriptions[user])
			subscriptions[user] = {};
		if(!subscriptions[user][d])
			subscriptions[user][d] = [];
		if(!subscriptions[user][d].find(_t => t === _t)){
			subscriptions[user][d].push(t);
			setSubscriptions(subscriptions);
			return true;
		}
		else
			return false;
	},
	unsubscribe: (user, d, t) => {
		let subscriptions = getSubscriptions();
		if(!subscriptions[user])
			return 3;
		if(!subscriptions[user][d])
			return 2;
		if(!subscriptions[user][d].find(_t => t === _t))
			return 1;
		subscriptions[user][d] = subscriptions[user][d].filter(_t => t !== _t);
		Object.keys(subscriptions[user]).forEach(d => { if(subscriptions[user][d].length === 0) delete subscriptions[user][d]; });
		Object.keys(subscriptions).forEach(user => { if(Object.keys(subscriptions[user]).length === 0) delete subscriptions[user] });
		setSubscriptions(subscriptions);
		return 0;
	},
	get: user => getSubscriptions()[user] || null
};