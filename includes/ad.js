const request = require('requestretry');

const ad = 'https://api.alldebrid.com/v4';

let token = null;

function formatBytes(a, b){ if(0 === a) return "0 Bytes"; let c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"], f = Math.floor(Math.log(a)/Math.log(c)); return parseFloat((a / Math.pow(c, f)).toFixed(d)) + " " + e[f] }

module.exports = {
	auth: (_token, callback) => {
		request(`${ad}/user?agent=ED&apikey=${_token}`, (err, res, body) => {
			let r = null;
			try {
				r = JSON.parse(body);
			}
			catch(e){
				callback(e.stack);
			}
			if(r['status'] === 'success'){
				if(r['data']['user']['isPremium'] === false)
					return callback('notPremium');
				else {
					token = _token;
					// token = r['token'];
					callback(true);
				}
			}
			else {
				switch(r['error']['code']){
					case 'AUTH_BAD_APIKEY':
						callback('wrongIdOrPass');
						break;
					default:
						callback(r['error']['code']);
						break;
				}
			}
		});
	},
	unrestrict: (link, callback) => {
		request(`${ad}/link/unlock?agent=ED&apikey=${token}&link=${link}`, (err, res, body) => {
			let r = JSON.parse(body);
			if(r['error'])
				return callback({ error: r['error']['code'] });
			callback({
				name: r['data']['filename'],
				size: formatBytes(r['data']['filesize']),
				link: r['data']['link']
			});
		});
	}
};