module.exports = {
	twoDigits: number => number.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }),
	capitalize: string => string[0].toUpperCase() + string.slice(1).toLowerCase(),
	code: string => `\`${string}\``,
	codeBlock: (content, lang) => `\`\`\`${lang || ''}\n${content}\`\`\``,
	mention: (user, raw) => `<@${raw ? user.id : user}> ${raw ? `\`[${user.username}#${user.discriminator}]\`` : ''}`,
	channel: chanId => `<#${chanId}>`,
	randomHexColor: () => '#' + Math.floor(Math.random() * 16777215).toString(16),
	intersect: (array1, array2) => array1.filter(value => -1 !== array2.indexOf(value)),
	n: ["\u0030\u20E3","\u0031\u20E3","\u0032\u20E3","\u0033\u20E3","\u0034\u20E3","\u0035\u20E3", "\u0036\u20E3","\u0037\u20E3","\u0038\u20E3","\u0039\u20E3"]
};