const request = require('requestretry');

const config = require('../config.json');

const show = require('./show.js'),
	 utils = require('./utils.js');

const predb = 'https://predb.ovh/api/v1';

module.exports = {
	search: (query, callback) => {
		request(`${predb}/?q=${query}`, (err, res, body) => {
			try {
				body = JSON.parse(body);
			}
			catch(e){
				console.error('Failed to parse PreDB');
			}
			if(body['data']['rowCount'] === 0)
				return callback(null);
			callback(body['data']['rows'].filter(r => r['name'].startsWith(query)));
		});
	},
	checkNew: (callback) => {
		let newReleases = [];
		show.getLastEpisode(episode => {
			module.exports.search(`${config.show.releaseName}.S${utils.twoDigits(episode['season'])}E${utils.twoDigits(episode['number'])}`, res => {
				if(!res) return callback(null);
				res.forEach(release => {
					if(new Date(release['preAt'] * 1000) > Date.now())
						newReleases.push(release['name']);
				});
				if(newReleases.length === 0) return callback(null);
				callback(newReleases);
			});
		});
	}
};