const Discord = require('discord.js');

const config = require('../config'),
	_package = require('../package.json'),
	   utils = require('./utils.js');

module.exports = (Client, properties) => {
	const Y = new Date().getFullYear();
	let i = 0;
	while(properties['fields'][i]['inline']){
		properties['fields'][i]['name'] = '** **\n' + properties['fields'][i]['name'];
		i++;
	}
	properties['fields'].forEach((item, index) => {
		if(item['condition'] && item['condition']() === false){
			properties['fields'].splice(index, 1);
		}
	});
	const E = new Discord.RichEmbed(Object.assign(properties, {
		timestamp: new Date(),
		author: {
			name: config.name,
			url: _package.repository.url || ''
		},
		footer: {
			icon_url: Client.user.avatarURL,
			text: _package.author ? `(C) ${config.year}${(Y > config.year ? '-' + Y : '')} ${_package.author.name || _package.author}` : ''
		}
	}));
	E.setColor(config.embedColor || utils.randomHexColor());
	for(let i = 0; i < E.fields.length; i++){
		if(E.fields[i] === 'margin'){
			E.fields[i] = { name: '** **', value: '** **' };
		}
	}
	return E;
};