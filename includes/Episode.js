const  fs = require('fs');

const config = require('../config.json');

const show = require('./show.js'),
	 embed = require('./embed.js'),
	 utils = require('./utils.js'),
 reactions = require('./reactions.js'),
		ad = require('./ad.js');

const dataPath = './data/downloads.json';

let getDownloads = () => JSON.parse(fs.readFileSync(dataPath, 'utf-8') || {});

module.exports = class Episode {
	constructor(s, e, Client, commandUser){
		this.s = s;
		this.e = e;
		this.commandUser = commandUser;
		this.Client = Client;
	}

	getData(callback){
		show.getEpisode(this.s, this.e, res => {
			this.updateData(res);
			callback();
		});
	}

	updateData(episode){
		if(!episode) return;
		this.s = episode['season'];
		this.e = episode['number'];
		this.se = `S${utils.twoDigits(this.s)}E${utils.twoDigits(this.e)}`;
		this.title = episode['name'] || 'Unknown';
		this.air_date = episode['airstamp'];
		if(!this.air_date){
			this.air_date = 'N/A';
		}
		else {
			this.air_date = new Date(this.air_date).toLocaleString('en-US', {
				weekday: 'long',
				year: 'numeric',
				month: 'long',
				day: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				timeZone: 'UTC',
				timeZoneName: 'short'
			}).replace('Invalid Date', 'N/A')
		}
		try {
			this.synopsis = episode['summary'].replace(/<\/?p>/g, '\n');
		}
		catch(e){
			this.synopsis = 'Unknown';
		}
		this.image = episode['image'] ? episode['image']['original'] : null;
		this.url = episode['url'] || null;
	}

	generateEmbed(){
		this.embed = embed(this.Client, {
			title: this.se,
			description: this.title,
			url: this.url,
			fields: [
				{
					name: 'Air date',
					value: this.air_date,
					inline: true
				},
				{
					name: 'Synopsis',
					value: this.synopsis
				}
			],
			image: {
				url: this.image
			}
		});
	}

	postEmbed(Client, channel){
		channel.send(this.embed).then(message => {
			this.message = message;
			if(this.commandUser){
				reactions.add(message, ['⏪', '◀', '▶', '⏩', config.show.alldebrid ? '💾' : false, '🗑'].filter(Boolean), () => {
					reactions.on(message.id, {
						'⏪': {
							action: cb => this.prevSeason(cb),
							remove: true,
							user: this.commandUser
						},
						'◀': {
							action: cb => this.prevEpisode(cb),
							remove: true,
							user: this.commandUser
						},
						'▶': {
							action: cb => this.nextEpisode(cb),
							remove: true,
							user: this.commandUser
						},
						'⏩': {
							action: cb => this.nextSeason(cb),
							remove: true,
							user: this.commandUser
						},
						'🗑': {
							action: () => message.delete(),
							user: this.commandUser
						},
						'💾': {
							action: () => this.downloadEpisode(),
							remove: true,
							user: this.commandUser
						}
					});
				});
			}
		}).catch(err => console.error(err));
	}

	updateEmbed(cb){
		this.generateEmbed();
		this.message.edit(this.embed)
			.then(() => { if(cb) cb(); })
			.catch(err => console.error(err));
	}

	prevEpisode(cb){
		show.getPrevEpisode(this.s, this.e, res => {
			this.updateData(res);
			this.updateEmbed(cb);
		});
	}

	nextEpisode(cb){
		show.getNextEpisode(this.s, this.e, res => {
			this.updateData(res);
			this.updateEmbed(cb);
		});
	}

	prevSeason(cb){
		show.getPrevSeasonEpisode(this.s, res => {
			this.updateData(res);
			this.updateEmbed(cb);
		});
	}

	nextSeason(cb){
		show.getNextSeasonEpisode(this.s, res => {
			this.updateData(res);
			this.updateEmbed(cb);
		});
	}

	downloadEpisode(){
		let downloads = null;
		try {
			downloads = getDownloads()[this.s][this.e];
		} catch(e){}
		if(!downloads)
			return this.message.channel.send(`:x: No download available for ${this.se}.`);
		this.message.channel.send(`Downloads available for ${this.se} :\n${utils.codeBlock(Object.keys(downloads).join('\n'))}`)
			.then(message => {
				reactions.add(message, utils.n.slice(1, Object.keys(downloads).length + 1), () => {
					let handler = {};
					Object.keys(downloads).forEach((dl, index) => {
						handler[utils.n[index + 1]] = {
							action: () => {
								message.channel.send('Processing, please wait...');
								ad.auth(config.show.alldebrid, res => {
									if(res !== true){
										switch(res){
											case 'notPremium':
											case 'wrongIdOrPass':
												console.error(`AllDebrid : ${res}`);
												message.channel.send(':x: Download provider access denied');
												break;
											case 'tooManyAttempts':
												console.error(`AllDebrid : ${res}`);
												message.channel.send(':x: Download provider blocked due to spam.');
												break;
										}
									}
									else {
										let i = 0;
										let f = () => {
											ad.unrestrict(downloads[dl][i], res => {
												if(res['error']){
													if(i < downloads[dl].length - 1){
														i++;
														f();
													}
													else {
														message.channel.send(':x: Link generation failed.');
														console.error(`AllDebrid : ${res['error']}`);
													}
													return;
												}
												message.guild.members.get(this.commandUser).send(`Here is the link you requested.\nFile : ${utils.code(res['name'])} (${res['size']})\n<${res['link']}>`)
													.then(() => {
														message.channel.send('Your link has successfully been generated and sent to you in DM.');
													})
													.catch(err => {
														message.channel.send(':x: Your link has successfully been generated, but I was not able to send it to you in DM.');
														console.error(err);
													});
											});
										};
										f();
									}
								});
							},
							user: this.commandUser
						}
					});
					reactions.on(message.id, handler);
				});
			});
	}
};
