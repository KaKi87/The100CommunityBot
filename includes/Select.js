const reactions = require('./reactions.js'),
	      utils = require('./utils.js');

module.exports = class Select {
	constructor(Client, channel, commandUser, list, selected, onCancel, onSubmit){
		this.channel = channel;
		this.commandUser = commandUser;
		this.list = list;
		this.selected = selected || 0;
		this.onCancel = onCancel;
		this.onSubmit = onSubmit;
		this.sendSelect();
	}

	generateSelect(){
		this.select = '';
		this.list.forEach((item, index) => {
			this.select += `${utils.code(`[ ${this.selected === index ? '■' : ' '} ]`)} ${item}\n`;
		});
		this.select += '\n** **';
	}

	sendSelect(cb){
		if(!this.select)
			this.generateSelect();
		this.channel.send(this.select).then(message => {
			reactions.add(message, ['❌', '⏫', '🔼', '🔽', '⏬', '✅'], () => {
				reactions.on(message.id, {
					'🔼': {
						action: cb => this.selectIndex(this.selected - 1, cb),
						remove: true,
						cb: true,
						user: this.commandUser
					},
					'🔽': {
						action: cb => this.selectIndex(this.selected + 1, cb),
						remove: true,
						cb: true,
						user: this.commandUser
					},
					'⏫': {
						action: cb => this.selectIndex(this.selected - 2, cb),
						remove: true,
						cb: true,
						user: this.commandUser
					},
					'⏬': {
						action: cb => this.selectIndex(this.selected + 2, cb),
						remove: true,
						cb: true,
						user: this.commandUser
					},
					'❌': {
						action: () => this.cancelSelect(),
						user: this.commandUser
					},
					'✅': {
						action: () => this.submitSelect(),
						user: this.commandUser
					}
				});
			});
			this.message = message;
			if(cb)
				cb();
		});
	}

	updateSelect(cb){
		this.message.edit(this.select).then(() => { if(cb) cb(); });
	}

	selectIndex(index, cb){
		if(index < 0 || index > this.list.length - 1) return cb();
		this.selected = index;
		this.generateSelect();
		if(!this.message)
			this.sendSelect();
		this.updateSelect(() => { if(cb) cb(); });
	}

	cancelSelect(){
		this.message.delete().then(() => {
			this.onCancel();
		});
	}

	submitSelect(){
		this.message.delete().then(() => {
			this.onSubmit(this.list[this.selected]);
		});
	}
};