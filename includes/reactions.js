let handler = {};

module.exports = {
	add: (message, reactions, callback) => {
		(async() => {
			for(let i = 0; i < reactions.length; i++){
				await message.react(reactions[i]);
			}
			callback();
		})().catch(err => console.error(err));
	},
	on: (message, handle) => {
		if(!handler[message])
			handler[message] = {};
		handler[message] = { ...handler[message], ...handle };
	},
	handle: (messageId, emoji, userId, remove) => {
		if(
			!handler[messageId]
			||
			!handler[messageId][emoji]
			||
			(handler[messageId][emoji].user && handler[messageId][emoji].user !== userId)
		) return;
		if(handler[messageId][emoji].cb){
			handler[messageId][emoji].action(() => {
				if(handler[messageId][emoji].remove)
					remove();
			});
		}
		else {
			handler[messageId][emoji].action();
			if(handler[messageId][emoji].remove)
				remove();
		}
	}
};
