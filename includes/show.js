const config = require('../config.json'),
   TVMazeApi = require('tvmaze'),
      tvMaze = new TVMazeApi();

let getAllEpisodes = callback => tvMaze.getEpisodes(config.show.tvMazeID).then(res => callback(res));
let getAllSeasons = callback => tvMaze.getSeasons(config.show.tvMazeID).then(res => callback(res));

module.exports = {
	getShow: callback => {
		tvMaze.getShow(config.show.tvMazeID).then(res => callback(res));
	},
	getEpisode: (s, e, callback) => {
		getAllEpisodes(episodes => {
			let res = episodes.find(episode => episode['season'] === s && episode['number'] === e);
			if(res && !res['image']){
				module.exports.getShow(show => {
					res['image'] = show['image'];
					callback(res);
				});
			}
			else {
				callback(res);
			}
		});
	},
	getSeason: (s, callback) => {
		getAllSeasons(seasons => callback(seasons.find(season => season['number'] === s)));
	},
	getLastEpisode: callback => {
		/*getAllEpisodes(episodes => {
			let today = new Date().setHours(0, 0, 0, 0);
			for(let i = episodes.length - 1; i > 0; i--){
				let airdate = new Date(episodes[i]['airdate']).setHours(0, 0, 0, 0);
				if(airdate <= today){
					return callback(episodes[i]);
				}
			}
		});*/
		module.exports.getShow(res => {
			tvMaze.getEpisodeById(res['_links']['previousepisode']['href'].split('/').slice(-1)[0]).then(res => {
				if(res && !res['image']){
					module.exports.getShow(show => {
						res['image'] = show['image'];
						callback(res);
					});
				}
				else {
					callback(res);
				}
			});
		});
	},
	getFutureEpisode: callback => {
		let today = Date.now();
		getAllEpisodes(episodes => {
			for(let i = 0; i < episodes.length; i++){
				let airstamp = episodes[i]['airstamp'];
				if(airstamp === null || new Date(airstamp) > today){
					if(!episodes[i]['image']){
						module.exports.getShow(show => {
							episodes[i]['image'] = show['image'];
							callback(episodes[i]);
						});
					}
					else {
						callback(episodes[i]);
					}
					break;
				}
			}
		});
	},
	getPrevEpisode: (s, e, callback) => {
		if(e === 1){
			if(s === 1)
				return callback(null);
			module.exports.getSeason(s - 1, _s => {
				module.exports.getEpisode(s - 1, _s['episodeOrder'], res => callback(res));
			});
		}
		else {
			module.exports.getEpisode(s, e - 1, res => callback(res));
		}
	},
	getNextEpisode: (s, e, callback) => {
		module.exports.getSeason(s, _s => {
			if(e < _s['episodeOrder']){
				module.exports.getEpisode(s, e + 1, res => callback(res));
			}
			else {
				module.exports.getSeason(s + 1, res => {
					if(!res)
						return callback(null);
					module.exports.getEpisode(s + 1, 1, res => callback(res));
				});
			}
		})
	},
	getPrevSeasonEpisode: (s, callback) => {
		module.exports.getEpisode(s - 1, 1, res => callback(res));
	},
	getNextSeasonEpisode: (s, callback) => {
		module.exports.getEpisode(s + 1, 1, res => callback(res));
	},
	getAllSeasonsNumbers: callback => {
		getAllSeasons(res => {
			callback(res.map(s => s['number']))
		});
	}
};